#ifndef CHORD_BOXES_CHORD_BOX_HPP
#define CHORD_BOXES_CHORD_BOX_HPP


#include <vector>
#include <string>
#include <sstream>
#include <iostream>


namespace chord_box {


struct String {
    typedef std::string Text;
    enum Display { Line, Dot };

    Text text;
    Display display;

    String(): text(""), display(Line) {}
};

struct Fret {
    typedef std::vector<String> Strings;

    Strings strings;

    Fret(): strings(6) {}
};

class ChordBox {
public:
    typedef std::vector<Fret> Frets;

    std::string  title;
    Frets frets;

    ChordBox():
        title("New Chord Box"), frets(4)
    {}
};


} // namespace chord_box


#endif // CHORD_BOXES_CHORD_BOX_HPP
