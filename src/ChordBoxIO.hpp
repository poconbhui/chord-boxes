#ifndef CHORD_BOX_CHORD_BOX_IO_HPP
#define CHORD_BOX_CHORD_BOX_IO_HPP

#include "ChordBox.hpp"

namespace chord_box {

const double box_height = 100;
const double box_width  = 100;

const double dot_width = 90;
const double font_size = 75;

const double line_thickness = 6;


template<typename Stream>
void write_string(const ChordBox& chord_box, Stream& stream) {
    // Find width for each string
    std::size_t string_width = 0;
    for(
        ChordBox::Frets::const_iterator f_it = chord_box.frets.begin();
        f_it != chord_box.frets.end();
        f_it++
    ) {
        for(
            Fret::Strings::const_iterator
                s_it = f_it->strings.begin();
            s_it != f_it->strings.end();
            s_it++
        ) {
            std::size_t local_string_width = 0;
            if(s_it->display == String::Line) {
                local_string_width = 3;
            } else {
                local_string_width =
                    2 + std::max<std::size_t>(s_it->text.length(), 1);
            }

            if(string_width < local_string_width) {
                string_width = local_string_width;
            }
        }
    }


    std::size_t n_strings = chord_box.frets[0].strings.size();
    std::size_t title_width = string_width*n_strings;
    int title_padding = static_cast<int>(title_width - chord_box.title.length())/2;
    title_padding = std::max(title_padding, 0);

    stream
        << std::string(title_padding, ' ')
        << chord_box.title
        << std::endl;


    for(
        ChordBox::Frets::const_iterator f_it = chord_box.frets.begin();
        f_it != chord_box.frets.end();
        f_it++
    ) {
        for(
            Fret::Strings::const_iterator
                s_it = f_it->strings.begin();
            s_it != f_it->strings.end();
            s_it++
        ) {
            std::string text;
            if(s_it->display == String::Line) {
                text = "|";
            } else {
                text = s_it->text;
                if(text.length() == 0) text = " ";
                text = "(" + text + ")";
            }

            int string_padding_l = static_cast<int>(
                string_width - text.length()
            )/2;
            string_padding_l = std::max(string_padding_l, 0);
            if(string_padding_l > 0) {
                text = std::string(string_padding_l, ' ') + text;
            }
            int string_padding_r = static_cast<int>(
                string_width - text.length()
            );
            if(string_padding_r > 0) {
                text = text + std::string(string_padding_r, ' ');
            }

            stream << text;
        }
        stream << std::endl;
    }
}


template<typename Stream>
void write_svg(const ChordBox& chord_box, Stream& stream) {
    double n_frets = static_cast<double>(chord_box.frets.size());
    double n_strings = 6;
    if(chord_box.frets.size() > 0) {
        n_strings = static_cast<double>(chord_box.frets[0].strings.size());
    }

    double total_height = (n_frets+1)*box_height + line_thickness/2;
    double total_width  = n_strings*box_width;

    stream
        << "<svg " << std::endl
        << "    xmlns=\"http://www.w3.org/2000/svg\"" << std::endl
        << "    xmlns:xlink=\"http://www.w3.org/1999/xlink\"" << std::endl
        << "    width=\"" << total_width << "\"" << std::endl
        << "    height=\"" << total_height << "\"" << std::endl
        << ">" << std::endl;

    // Styles
    stream
        << "<style type=\"text/css\">" << std::endl
        << "<![CDATA[" << std::endl
        << "    text.title, text.dot {" << std::endl
        << "        font-size: " << font_size << "px;" << std::endl
        << "        font-family: arial, sans-serif;"
        << "        dominant-baseline: central;" << std::endl
        << "        fill: #000000;"
        << "    }" << std::endl
        << "    text.dot {" << std::endl
        << "        fill: #ffffff;" << std::endl
        << "    }" << std::endl
        << "    line.fret, line.string {"
        << "        stroke: #000000;"
        << "        stroke-width: 5px;"
        << "    }"
        << "    circle.dot {"
        << "        fill: #000000;"
        << "    }"
        << "]]>" << std::endl
        << "</style>" << std::endl;

    // Print title
    stream
        << "<text class=\"title\"" << std::endl
        << "    x=\"" << total_width/2 << "\"" << std::endl
        << "    y=\"" << box_height/2 << "\"" << std::endl
        << "    style=\"" << std::endl
        << "        text-anchor: middle;" << std::endl
        << "    \"" << std::endl
        << ">"
        << chord_box.title
        << "</text>" << std::endl;

    // Print Fret lines
    for(std::size_t f_i = 0; f_i < n_frets+1; f_i++) {
        double x_pos = box_width/2 - line_thickness/2;
        double y_pos = (f_i+1)*box_height - line_thickness/2;

        double width = (n_strings-1)*box_width + line_thickness;
        double height = line_thickness;

        stream
            << "<rect class=\"fret\"" << std::endl
            << "    x=\"" << x_pos << "\"" << std::endl
            << "    y=\"" << y_pos << "\"" << std::endl
            << "    width=\"" << width << "\"" << std::endl
            << "    height=\"" << height << "\"" << std::endl
            << "/>" << std::endl;
    }

    // Print String Lines
    for(std::size_t s_i = 0; s_i < n_strings; s_i++) {
        double x_pos = box_width/2 + s_i*box_width - line_thickness/2;
        double y_pos = box_height - line_thickness/2;

        double height = n_frets*box_height + line_thickness;
        double width = line_thickness;

        stream
            << "<rect class=\"fret\"" << std::endl
            << "    x=\"" << x_pos << "\"" << std::endl
            << "    y=\"" << y_pos << "\"" << std::endl
            << "    width=\"" << width << "\"" << std::endl
            << "    height=\"" << height << "\"" << std::endl
            << "/>" << std::endl;
    }

    // Print dots and text
    for(std::size_t f_i = 0; f_i < chord_box.frets.size(); f_i++) {
        const Fret& fret = chord_box.frets[f_i];
        for(std::size_t s_i = 0; s_i < fret.strings.size(); s_i++) {
            const String& string = fret.strings[s_i];

            if(string.display == String::Dot) {
                double x_pos = box_width/2 + s_i*box_width;
                double y_pos = (f_i+1)*box_height + box_height/2;

                // Dot
                stream
                    << "<circle class=\"dot\"" << std::endl
                    << "    cx=\"" << x_pos << "\"" << std::endl
                    << "    cy=\"" << y_pos << "\"" << std::endl
                    << "    r=\"" << dot_width/2 << "\"" << std::endl
                    << "/>" << std::endl;

                // Text
                stream
                    << "<text class=\"dot\"" << std::endl
                    << "    x=\"" << x_pos << "\"" << std::endl
                    << "    y=\"" << y_pos << "\"" << std::endl
                    << "    style=\"" << std::endl
                    << "        text-anchor: middle;" << std::endl
                    << "    \"" << std::endl
                    << ">"
                    << string.text
                    << "</text>" << std::endl;
            }
        }
    }


    stream
        << "</svg>" << std::endl;
}

} // namespace chord_box

#endif // CHORD_BOX_CHORD_BOX_IO_HPP
