#include "ChordBoxWidget.hpp"

#include <QApplication>
#include <QDesktopWidget>

#include "signal.h"

void quit(int signo) {
    QApplication::instance()->quit();
}

int main(int argc, char* argv[]) {
    signal(SIGINT, quit);

    QApplication app(argc, argv);

    chord_box::ChordBoxWindow window;

    window.setMinimumSize(500, 450);

    QSize initial_size = QDesktopWidget().availableGeometry(&window).size()*0.7;
    window.resize(initial_size.width(), initial_size.height());

    window.show();

    return app.exec();
}
