#ifndef CHORD_BOXES_CHORD_BOX_WIDGET_HPP
#define CHORD_BOXES_CHORD_BOX_WIDGET_HPP

#include "ChordBox.hpp"
#include "ChordBoxIO.hpp"

#include <QWidget>
#include <QFrame>
#include <QPushButton>
#include <QResizeEvent>
#include <QPainter>
#include <QApplication>
#include <QLineEdit>
#include <QDrag>
#include <QMimeData>
#include <QFileDialog>

#include <vector>
#include <sstream>
#include <fstream>


namespace chord_box {


class StringWidget: public QFrame {
    Q_OBJECT
public:
    String& string;
    bool caughtMousePress;
    QLineEdit* text_editor;

    explicit StringWidget(
        String& string, QWidget* parent = 0
    ):
        QFrame(parent),
        string(string),
        caughtMousePress(false),
        text_editor(new QLineEdit(this))
    {
        setProperty("name", "StringWidget");

        setStyleSheet();

        text_editor->hide();
        text_editor->setAlignment(Qt::AlignCenter);
        text_editor->setAttribute(Qt::WA_TranslucentBackground, true);
        text_editor->setProperty("name", "StringWidget::text_editor");

        connect(
            text_editor, SIGNAL(editingFinished()),
            this, SLOT(slotUpdateText())
        );

        setMouseTracking(true);
    }

    void setStyleSheet() {
        int font_size = static_cast<int>(
            chord_box::font_size * (
                static_cast<double>(height()) / chord_box::box_height
            )
        );
        font_size = std::max(font_size, 1);

        int line_thickness = static_cast<int>(
            chord_box::line_thickness * (
                static_cast<double>(width()) / chord_box::box_width
            )
        );

        std::stringstream ss;
        ss
            << "QFrame[name=\"StringWidget\"] {"
            << "    background: none;       "
            << "    color: white;           "
            << "    font-size: " << font_size << "px;"
            << "}                           "
            << "                            "
            << "QFrame[name=\"StringWidget\"]:hover {"
            << "    background-color: #EEAA7B; "
            << "    border: " << line_thickness/2 << "px solid black;"
            << "}                           "
            << "QLineEdit[name=\"StringWidget::text_editor\"] {"
            << "    background-color: rgba(0,0,0,0);"
            << "    color: white;"
            << "    font-size: " << font_size << "px;"
            << "}";

        QFrame::setStyleSheet(ss.str().c_str());
    }

    void resizeEvent(QResizeEvent* event) {
        setStyleSheet();
    }

    void paintEvent(QPaintEvent* event) {
        // Paint background
        QWidget::paintEvent(event);

        QPainter painter;

        // Paint string
        painter.begin(this);
        QPen pen = painter.pen();

        int line_thickness = static_cast<int>(
            chord_box::line_thickness * (
                static_cast<double>(width()) / chord_box::box_width
            )
        );

        painter.setPen(Qt::NoPen);
        painter.setBrush(Qt::black);
        painter.drawRect(QRect(
            QPoint(width()/2 - line_thickness/2, 0),
            QPoint(width()/2 + line_thickness/2, height())
        ));
        painter.end();

        // Paint finger dot and text
        if(string.display == String::Dot) {
            // Draw circle
            painter.begin(this);
            painter.setBrush(Qt::black);
            painter.setPen(Qt::NoPen);
            std::size_t dot_width = static_cast<int>(
                chord_box::dot_width * (
                    static_cast<double>(width())/chord_box::box_width
                )
            );
            std::size_t padding = (width() - dot_width)/2;
            painter.drawEllipse(
                QPointF(width()/2, height()/2),
                dot_width/2, dot_width/2
            );
            painter.end();

            // Draw text
            painter.begin(this);
            painter.drawText(
                QRectF(0, 0, width(), height()),
                Qt::AlignCenter,
                tr(string.text.c_str())
            );
            painter.end();
        }
    }


    void mousePressEvent(QMouseEvent* event) {
        if(event->button() == Qt::LeftButton) {
            caughtMousePress = true;
        } else {
            showTextEditor();
        }
    }
    void mouseReleaseEvent(QMouseEvent* event) {
        if(caughtMousePress == true) {
            caughtMousePress = false;
            slotButtonClicked();
        }
    }
    void mouseMoveEvent(QMouseEvent* event) {
        caughtMousePress = false;
        event->ignore();
    }
    void mouseDoubleClickEvent(QMouseEvent* event) {
        if(event->button() == Qt::LeftButton) {
            showTextEditor();
        }

        update();
    }

    void enterEvent(QEvent* event) {
        setFocus(Qt::MouseFocusReason);
    }

    void keyPressEvent(QKeyEvent* event) {

        switch(event->key()) {
            case Qt::Key_Return:
                if(text_editor->isHidden()) {
                    showTextEditor();
                } else {
                    hideTextEditor();
                }
                break;

            case Qt::Key_Escape:
                hideTextEditor();
                break;

            default:
                showTextEditor();
                text_editor->event(event);
        }
    }

    void showTextEditor() {
        string.text = "";
        string.display = String::Dot;

        text_editor->setGeometry(1, 0, width(), height());
        text_editor->show();
        text_editor->setFocus();
    }

    void hideTextEditor() {
        text_editor->hide();
    }

public slots:
    void slotButtonClicked() {
        if(string.display == String::Line) {
            string.display = String::Dot;
        } else {
            string.display = String::Line;
        }
        update();
    }

    void slotUpdateText() {
        string.text = text_editor->text().toStdString();

        update();
    }
};

class FretWidget: public QFrame {
    Q_OBJECT
public:
    typedef std::vector<StringWidget*> StringWidgets;

    Fret& fret;

    StringWidgets string_widgets;

    explicit FretWidget(
        Fret& fret, QWidget *parent = 0
    ):
        QFrame(parent),
        fret(fret),
        string_widgets()
    {
        setProperty("name", "FretWidget");
        setStyleSheet(
            "QFrame[name=\"FretWidget\"] {"
            "   background-color: rgba(0,0,0,0);"
            "}"
        );

        for(std::size_t i=0; i < fret.strings.size(); i++) {
            string_widgets.push_back(
                new StringWidget(
                    fret.strings[i], this
                )
            );
        }
    }

    void resizeEvent(QResizeEvent *event) {
        int height = event->size().height();
        int width  = event->size().width();

        int box_width = width / static_cast<int>(string_widgets.size());
        box_width = std::min(box_width, height);

        for(std::size_t i=0; i < string_widgets.size(); i++) {
            string_widgets[i]->setGeometry(
                static_cast<int>(i)*box_width, 0,
                box_width, box_width
            );
        }
    }

    QPoint dragStartPosition;
    void mousePressEvent(QMouseEvent* event) {
        if(event->button() == Qt::LeftButton) {
            dragStartPosition = event->pos();

        }
    }

    void mouseMoveEvent(QMouseEvent* event) {
        if (!(event->buttons() & Qt::LeftButton)) {
            return;
        }
        if (
            (event->pos() - dragStartPosition).manhattanLength()
                < QApplication::startDragDistance()
        ) {
            return;
        }

        QDrag* drag = new QDrag(this);
        QMimeData* mimeData = new QMimeData;

        mimeData->setText("application/x-qdrag-chord-box-row");
        drag->setMimeData(mimeData);

        QCursor drag_cursor(Qt::WaitCursor);
        drag->setDragCursor(drag_cursor.pixmap(), Qt::MoveAction);

        Qt::DropAction dropAction = drag->exec(Qt::MoveAction, Qt::MoveAction);
    }


    void paintEvent(QPaintEvent* event) {
        // Paint background
        QWidget::paintEvent(event);

        QPainter painter;

        // Paint borders
        int box_width = width() / static_cast<int>(string_widgets.size());
        int line_thickness = static_cast<int>(
            chord_box::line_thickness * (
                static_cast<double>(height())/chord_box::box_height
            )
        );

        painter.begin(this);
        painter.setPen(Qt::NoPen);
        painter.setBrush(Qt::black);
        painter.drawRect(
            box_width/2, 0,
            width() - box_width, line_thickness/2
        );
        painter.drawRect(
            box_width/2, height() - line_thickness/2,
            width() - box_width, line_thickness/2
        );
        painter.end();
    }
};


class ChordBoxWidget: public QFrame {
    Q_OBJECT
public:
    typedef std::vector<FretWidget*> FretWidgets;

    ChordBox chord_box;

    QLineEdit* title;
    FretWidgets fret_widgets;

    explicit ChordBoxWidget(QWidget* parent = 0):
        QFrame(parent),
        chord_box(),
        fret_widgets()
    {
        setProperty("name", "ChordBoxWidget");

        setFrameStyle(Box);

        setAcceptDrops(true);

        title = new QLineEdit(chord_box.title.c_str(), this);
        title->setAlignment(Qt::AlignCenter);
        title->setFrame(false);
        title->show();
        title->setProperty("name", "ChordBoxWidget::title");

        connect(
            title, SIGNAL(editingFinished()),
            this, SLOT(slotUpdateTitle())
        );

        for(std::size_t i=0; i < chord_box.frets.size(); i++) {
            fret_widgets.push_back(
                new FretWidget(
                    chord_box.frets[i], this
                )
            );
        }

        setStyleSheet();
    }


    void resizeEvent(QResizeEvent *event) {
        resizeChildren();
    }

    void resizeChildren() {
        int min_padding = 10;

        int n_strings = static_cast<int>(
            fret_widgets[0]->string_widgets.size()
        );
        int n_frets = static_cast<int>(fret_widgets.size());

        int box_height = (height() - min_padding) / (n_frets + 2);
        int box_width_0 = (width() - min_padding) / n_strings;
        int min_len = std::min(box_height, box_width_0);

        box_height = min_len;
        int box_width = min_len*n_strings;

        // Center rows
        int h_padding = (height() - (n_frets+2)*box_height)/2;
        int w_padding = (width() - box_width)/2;


        title->setGeometry(
            w_padding, h_padding,
            box_width, box_height
        );

        for(std::size_t i=0; i < fret_widgets.size(); i++) {
            fret_widgets[i]->setGeometry(
                w_padding, h_padding + (static_cast<int>(i)+1)*box_height,
                box_width, box_height
            );
        }

        setStyleSheet();
    }

    void setStyleSheet() {
        QFrame::setStyleSheet(
            "QFrame[name=\"ChordBoxWidget\"] {"
            "   background-color: white;"
            "}"
        );

        int font_size = title->height()*0.75;
        font_size = std::max(font_size, 1);

        std::stringstream ss;
        ss
            << "QLineEdit[name=\"ChordBoxWidget::title\"] {"
            << "    color: black;"
            << "    background-color: rgba(0,0,0,0);"
            << "    font-size: " << font_size << "px;"
            << "    font-family: arial, sans-serif;"
            << "}";
        title->setStyleSheet(ss.str().c_str());

    }

    void paintEvent(QPaintEvent* event) {

        // Add upper and lower borders for Frets

        // Get top and bottom fret widgets
        FretWidget* top_fret_widget = fret_widgets[0];
        FretWidget* bottom_fret_widget = fret_widgets.back();

        // Get top and bottom position of frets
        QPoint top = top_fret_widget->mapToParent(QPoint(0,0));
        QPoint bottom = bottom_fret_widget->mapToParent(
            QPoint(0, bottom_fret_widget->height())
        );

        // Get the center position of the leftmost and rightmost
        // string widgets
        QPoint left = top_fret_widget->mapToParent(
            top_fret_widget->string_widgets[0]->mapToParent(
                QPoint(top_fret_widget->string_widgets[0]->width()/2, 0)
            )
        );
        QPoint right = top_fret_widget->mapToParent(
            top_fret_widget->string_widgets.back()->mapToParent(
                QPoint(top_fret_widget->string_widgets.back()->width()/2, 0)
            )
        );

        int line_thickness = static_cast<int>(
            chord_box::line_thickness * (
                static_cast<double>(top_fret_widget->height())
                / chord_box::box_height
            )
        );

        QPainter painter(this);
        painter.setPen(Qt::NoPen);
        painter.setBrush(Qt::black);
        painter.drawRect(QRect(
            QPoint(left.x()  - line_thickness/2, top.y() - line_thickness/2),
            QPoint(right.x() + line_thickness/2, top.y())
        ));
        painter.drawRect(QRect(
            QPoint(left.x()  - line_thickness/2, bottom.y() + line_thickness/2),
            QPoint(right.x() + line_thickness/2, bottom.y())
        ));

        QFrame::paintEvent(event);
    }

    void dragEnterEvent(QDragEnterEvent* event) {
        QObject* source = event->source();
        for(std::size_t i=0; i < fret_widgets.size(); i++) {
            if(source == fret_widgets[i]) {
                event->acceptProposedAction();
            }
        }
    }

    void dragMoveEvent(QDragMoveEvent* event) {
        FretWidget* source = dynamic_cast<FretWidget*>(
            event->source()
        );

        if(!source) return;

        // Get the child the cursor is currently hovering over
        QWidget* child = childAt(event->pos());

        // childAt seems to be returning StringWidget
        // get the FretWidget that contains it.
        if(dynamic_cast<StringWidget*>(child)) {
            child = child->parentWidget();
        }
        if(!dynamic_cast<FretWidget*>(child)) {
            event->ignore();
            return;
        }

        // Find i for child under cursor
        int i;
        for(i = 0; i < fret_widgets.size(); i++) {
            if(fret_widgets[i] == child) break;
        }

        // Find j for source
        int j;
        for(j = 0; j < fret_widgets.size(); j++) {
            if(fret_widgets[j] == source) break;
        }

        int n_cbrw = static_cast<int>(fret_widgets.size());
        if(i < n_cbrw && j < n_cbrw) {
            std::swap(
                fret_widgets[i], fret_widgets[j]
            );
            std::swap(
                chord_box.frets[i], chord_box.frets[j]
            );
            resizeChildren();
            update();
        }

        event->acceptProposedAction();
    }

public slots:
    void slotPrintString() {
        std::cout << std::endl;
        std::cout << "write_string" << std::endl;
        write_string(chord_box, std::cout);
        std::cout << std::endl;
    }

    void slotOutputToSVG() {
        QString filename = QFileDialog::getSaveFileName(
            this,
            tr("Save Chord"), "chord.svg", tr("SVG Files (*.svg)")
        );
        filename = filename.trimmed();
        if(!filename.isEmpty()) {
            if(!filename.endsWith(".svg")) {
                filename = filename + ".svg";
            }
            std::ofstream test_svg(filename.toStdString());
            write_svg(chord_box, test_svg);
        }
    }

    void slotAddFret() {
        chord_box.frets.push_back(Fret());
        fret_widgets.push_back(
            new FretWidget(chord_box.frets.back(), this)
        );
        fret_widgets.back()->show();
        resizeChildren();
        update();
    }

    void slotPopFret() {
        if(fret_widgets.size() == 1) return;

        QWidget* child = fret_widgets.back();
        fret_widgets.pop_back();
        chord_box.frets.pop_back();

        delete child;

        resizeChildren();
        update();
    }

    void slotUpdateTitle() {
        chord_box.title = title->text().toStdString();
    }
};



class ChordBoxWindow: public QFrame {
    Q_OBJECT
public:
    //const std::size_t button_padding = 15;
    const int button_padding = 0;
    const int button_width = 80;
    const int button_height = 65;

    ChordBoxWidget* chord_box_widget;

    QPushButton* print_button;
    QPushButton* add_row_button;
    QPushButton* del_row_button;
    QPushButton* save_button;
    QPushButton* quit_button;

    explicit ChordBoxWindow(QWidget* parent = 0):
        QFrame(parent)
    {
        setProperty("name", "ChordBoxWindow");

        chord_box_widget = new ChordBoxWidget(this);

        print_button = new QPushButton(this);
        print_button->setText("Print");
        print_button->setToolTip("Print Text Representation");
        //print_button->setFlat(true);

        QObject::connect(
            print_button, SIGNAL(clicked()),
            chord_box_widget, SLOT(slotPrintString())
        );
        print_button->hide();


        add_row_button = new QPushButton(this);
        add_row_button->setText("+");
        add_row_button->setToolTip("Add New Row");
        //add_row_button->setFlat(true);

        QObject::connect(
            add_row_button, SIGNAL(clicked()),
            chord_box_widget, SLOT(slotAddFret())
        );

        del_row_button = new QPushButton(this);
        del_row_button->setText("-");
        del_row_button->setToolTip("Remove Row");
        //del_row_button->setFlat(true);

        QObject::connect(
            del_row_button, SIGNAL(clicked()),
            chord_box_widget, SLOT(slotPopFret())
        );

        save_button = new QPushButton(this);
        save_button->setText("Save");
        save_button->setToolTip("Save Chord Box to File");

        QObject::connect(
            save_button, SIGNAL(clicked()),
            chord_box_widget, SLOT(slotOutputToSVG())
        );


        quit_button = new QPushButton(this);
        quit_button->setText("Quit");
        quit_button->setToolTip("Quit Application");
        //quit_button->setFlat(true);

        QObject::connect(
            quit_button, SIGNAL(clicked()),
            QApplication::instance(), SLOT(quit())
        );


        setStyleSheet();
    }

    void setStyleSheet() {
        int box_height = print_button->height();
        int font_size = box_height*0.6;
        font_size = std::max(font_size, 1);

        std::stringstream ss;
        ss
            << "QFrame[name=\"ChordBoxWindow\"] {"
            << "   background-color: #e0e0e0;"
            << "}";

        QFrame::setStyleSheet(ss.str().c_str());
    }

    void resizeChildren() {
        int chord_box_size = width() - button_width;

        chord_box_widget->setGeometry(0, 0, chord_box_size, height());

        print_button->setGeometry(
            chord_box_size + button_padding,
            0*(button_height) + button_padding,
            button_width - 2*button_padding, button_height - 2*button_padding
        );
        add_row_button->setGeometry(
            chord_box_size + button_padding + 0*button_width/2,
            0*(button_height) + button_padding,
            button_width/2 - button_padding, button_height - 2*button_padding
        );
        del_row_button->setGeometry(
            chord_box_size + button_padding + 1*button_width/2 - button_padding,
            0*(button_height) + button_padding,
            button_width/2 - button_padding, button_height - 2*button_padding
        );
        save_button->setGeometry(
            chord_box_size + button_padding,
            1*(button_height) + button_padding,
            button_width - 2*button_padding, button_height - 2*button_padding
        );
        quit_button->setGeometry(
            chord_box_size + button_padding,
            height() - button_height-1,
            button_width - 2*button_padding, button_height - 2*button_padding
        );
    }

    void resizeEvent(QResizeEvent *event) {
        resizeChildren();
    }
};


} // namespace chord_box


#endif // CHORD_BOXES_CHORD_BOX_WIDGET_HPP
